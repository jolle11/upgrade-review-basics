// Iteracion #1 - Mix for e includes

const movies = [
    { title: 'Madaraspar', duration: 192, categories: ['comedia', 'aventura'] },
    { title: 'Spiderpan', duration: 122, categories: ['aventura', 'acción'] },
    { title: 'Solo en Whatsapp', duration: 223, categories: ['comedia', 'thriller'] },
    { title: 'El gato con guantes', duration: 111, categories: ['comedia', 'aventura', 'animación'] },
];

const categorias = [];

for (const mov of movies) {
    for (let i = 0; i < mov.categories.length; i++) {
        if (categorias.includes(mov.categories[i]) == false) {
            categorias.push(mov.categories[i]);
        }
    }
}
console.log(categorias);

// Iteracion #2 - Mix Fors

const users = [
    {
        name: 'Manolo el del bombo',
        favoritesSounds: {
            waves: { format: 'mp3', volume: 50 },
            rain: { format: 'ogg', volume: 60 },
            firecamp: { format: 'mp3', volume: 80 },
        },
    },
    {
        name: 'Mortadelo',
        favoritesSounds: {
            waves: { format: 'mp3', volume: 30 },
            shower: { format: 'ogg', volume: 55 },
            train: { format: 'mp3', volume: 60 },
        },
    },
    {
        name: 'Super Lopez',
        favoritesSounds: {
            shower: { format: 'mp3', volume: 50 },
            train: { format: 'ogg', volume: 60 },
            firecamp: { format: 'mp3', volume: 80 },
        },
    },
    {
        name: 'El culebra',
        favoritesSounds: {
            waves: { format: 'mp3', volume: 67 },
            wind: { format: 'ogg', volume: 35 },
            firecamp: { format: 'mp3', volume: 60 },
        },
    },
];

// MARIO'S WAY 1

// VARIABLE TO SUM UP ALL THE VOLUMES
let suma = 0;
// VARIABLE TO COUNT THE NUMBER OF ITERATIONS
let itCount = 0;

// 1 - WE ITERATE THE ARRAY WITH A FOREACH LOOP
users.forEach(user => {
    // 2 - WE CREATE A CONST WHICH CONTAINS THE USER FAVORITE SOUNDS
    const sounds = user.favoritesSounds;
    // 3 - WE CREATE A CONST WHICH CONTAINS THE SOUND VALUES
    const listOfValues = Object.values(sounds);
    // 4 - WE ITERATE THE VALUES WITH A FOREACH LOOP
    listOfValues.forEach(value => {
        // 5 - WE SUM UP THE VALUE.VOLUME TO THE SUM VARIABLE
        suma += value.volume;
        // 6 - WE ADD 1 TO THE ITERATION COUNT VARIABLE
        itCount++;
    });
});

// MARIO'S WAY 2

// 1 - WE ITERATE THE ARRAY WITH A FOREACH LOOP
users.forEach(user => {
    // 2 - WE CREATE A CONST WHICH CONTAINS THE USER FAVORITE SOUNDS
    const sounds = user.favoritesSounds;
    // 3 - WE CREATE A CONST WHICH CONTAINS THE SOUND KEYS
    const keyOfSounds = Object.keys(sounds);
    // 4 - WE ITERATE THE KEYS WITH A FOREACH LOOP
    keyOfSounds.forEach(key => {
        // 5 - WE SUM UP THE KEY.VOLUME TO THE SUM VARIABLE
        suma += sounds[key].volume;
        // 6 - WE ADD 1 TO THE ITERATION COUNT VARIABLE
        itCount++;
    });
});

console.log(suma / itCount);

// Iteracion #3 - Mix Fors

// ARRAY THAT WILL CONTAIN ALL THE FAVORITE SOUNDS
const sonidos = [];
// OBJECT THAT WILL CONTAIN THE FINAL COUNT (NAME: NUMBER OF OCCURRENCIES)
const contador = {};
// WE ITERATE THE USERS ARRAY
for (const user of users) {
    // WE CREATE A CONST WHICH CONTAINS THE USER FAVORITE SOUNDS
    const sounds = user.favoritesSounds;
    // WE CREATE A CONST WHICH CONTAINS THE SOUND KEYS
    const keyOfSounds = Object.keys(sounds);
    // WE ITERATE THE KEYOFSOUNDS TO REACH EVERY KEY
    for (let i = 0; i < keyOfSounds.length; i++) {
        //WE PUSH EVERY VALUE TO THE SONIDOS ARRAY TO GET ALL VALUES IN ONE
        sonidos.push(keyOfSounds[i]);
    }
}
// WE SORT THE ARRAY
sonidos.sort();
// WE ITERATE THE SONIDOS ARRAY TO COUNT THE OCURRENCIES
for (let i = 0; i < sonidos.length; i++) {
    // RESULT FOR THE NEXT LINE WILL BE >> Firecamp = i(=0) =1
    contador[sonidos[i]] = contador[sonidos[i]] + 1 || 1;
}

console.log(contador);

// Iteracion #4 - Métodos findArrayIndex

const bichos = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'];

function findArrayIndex(array, text) {
    if (array.includes(text)) {
        console.log(array.indexOf(text));
    }
}

findArrayIndex(bichos, 'Salamandra');

// Iteracion #5 - Función rollDice

let faces = 6;

function rollDice(numFaces) {
    console.log(Math.floor(Math.random() * numFaces + 1));
}

rollDice(faces);

// Iteracion #6 - Función swap

const players = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño'];

function swap(array, i, j) {
    console.log(array.length);
    if (i < array.length && j < array.length) {
        var b = array[i];
        array[i] = array[j];
        array[j] = b;
        console.log(array);
    } else {
        console.log('Introduce numeros validos');
    }
}

swap(players, 1, 3);
